package fr.grunberg.musicothèque;

import static java.nio.file.StandardCopyOption.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.StreamSupport;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v1Tag;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.ID3v24Tag;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.NotSupportedException;
import com.mpatric.mp3agic.UnsupportedTagException;

public class Musicothèque {
	private static final boolean UTILISERID3V1 = true;
	private static final boolean MODIFIERFICHIERS = true;
	
	private static final String ATTRIBUT_ARTISTE = "ARTISTE";
	private static final String ATTRIBUT_CHANSON = "CHANSON";
	private static final String ATTRIBUT_ALBUM = "ALBUM";
	private static final String ATTRIBUT_ANNEE = "ANNEE";
	
	public static void main(String[] args) {
		new Musicothèque().exécution(args);
	}

	/**
	 * Méthode de base
	 * @param args
	 */
	public void exécution (String[] args) {
		if(args == null || args.length == 0) {
			System.err.println("Paramètres manquants");
		}
		Arrays.stream(args)
			.map(a -> a.replace("-t=", ""))
			.forEach(this::analyserChemin)
			;
	}
	
	private void analyserChemin(String chemin) {
		Path pathChemin = Paths.get(chemin);
		if(!Files.exists(pathChemin))
			return;
		if(Files.isRegularFile(pathChemin))
			modifierAttributsFichier(chemin);
		if(Files.isDirectory(pathChemin))
			try {
				StreamSupport.stream(
					Files.newDirectoryStream(pathChemin).spliterator(), false)
					.map(Path::toString)
					.forEach(this::analyserChemin)
					;
			} catch (IOException e) {
				System.err.println("Erreur lors de la lecture de " + pathChemin.toString());
				e.printStackTrace();
			}
	}
	
	private void modifierAttributsFichier(String cheminEtNomFichier) {
		Path pathFichierMP3 = Paths.get(cheminEtNomFichier);
		String nomFichierMP3Temporaire = cheminEtNomFichier + ".2.mp3";
		Path pathFichierMP3Temporaire = Paths.get(nomFichierMP3Temporaire);
		System.out.println("Traitement de " + getNomFichier(cheminEtNomFichier));
		try {
			Mp3File fichierMP3 = new Mp3File(cheminEtNomFichier);
			String nomFichier = this.getNomFichier(cheminEtNomFichier);
			Map<String, String> attributs = new HashMap<>();
			getChansonDepuisNom(nomFichier, attributs);
			getArtisteDepuisNom(nomFichier, attributs);
			if(fichierMP3.hasId3v1Tag()) {
				getAttributDepuisTag(attributs, ATTRIBUT_CHANSON, fichierMP3.getId3v1Tag().getTitle());
				getAttributDepuisTag(attributs, ATTRIBUT_ARTISTE, fichierMP3.getId3v1Tag().getArtist());
				getAttributDepuisTag(attributs, ATTRIBUT_ALBUM, fichierMP3.getId3v1Tag().getAlbum());
				getAttributDepuisTag(attributs, ATTRIBUT_ANNEE, fichierMP3.getId3v1Tag().getYear());
			}
			if(fichierMP3.hasId3v2Tag()) {
				getAttributDepuisTag(attributs, ATTRIBUT_CHANSON, fichierMP3.getId3v2Tag().getTitle());
				getAttributDepuisTag(attributs, ATTRIBUT_ARTISTE, fichierMP3.getId3v2Tag().getAlbumArtist());
				getAttributDepuisTag(attributs, ATTRIBUT_ALBUM, fichierMP3.getId3v2Tag().getAlbum());
				getAttributDepuisTag(attributs, ATTRIBUT_ANNEE, fichierMP3.getId3v2Tag().getDate());
			}
			System.out.println("\tChanson : " + attributs.get(ATTRIBUT_CHANSON));
			System.out.println("\tArtiste : " + attributs.get(ATTRIBUT_ARTISTE));
			System.out.println("\tAlbum : " + attributs.get(ATTRIBUT_ALBUM));
			System.out.println("\tAnnée : " + attributs.get(ATTRIBUT_ANNEE));
			if(attributs.containsKey(ATTRIBUT_ARTISTE) && attributs.containsKey(ATTRIBUT_CHANSON)) {
				if(UTILISERID3V1)
					mettreAttributs(basculerID3v1(fichierMP3), attributs);
				else
					mettreAttributs(basculerID3v2(fichierMP3), attributs);

				if(MODIFIERFICHIERS) {
					fichierMP3.save(nomFichierMP3Temporaire);
					Files.delete(pathFichierMP3);
					Files.move(pathFichierMP3Temporaire
							, pathFichierMP3
							, REPLACE_EXISTING);
				}
			}
		} catch (UnsupportedTagException | InvalidDataException | IOException | NotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	private void mettreAttributs(ID3v1 tags, Map<String, String> attributs) {
		tags.setTitle(attributs.get(ATTRIBUT_CHANSON));
		tags.setArtist(attributs.get(ATTRIBUT_ARTISTE));
		tags.setYear(attributs.get(ATTRIBUT_ANNEE));
		tags.setAlbum(attributs.get(ATTRIBUT_ALBUM));
	}
	
	private void mettreAttributs(ID3v2 tags, Map<String, String> attributs) {
		tags.setTitle(attributs.get(ATTRIBUT_CHANSON));
		tags.setAlbumArtist(attributs.get(ATTRIBUT_ARTISTE));
		tags.setDate(attributs.get(ATTRIBUT_ANNEE));
		tags.clearAlbumImage();
	}
	
	private ID3v1 basculerID3v1(Mp3File fichierMP3) {
		if(fichierMP3.hasCustomTag())
			fichierMP3.removeCustomTag();
		if(!fichierMP3.hasId3v1Tag()) {
			ID3v1Tag id3Tag = new ID3v1Tag();
			fichierMP3.setId3v1Tag(id3Tag);
		}
		if(fichierMP3.hasId3v2Tag())
			fichierMP3.removeId3v2Tag();
		return fichierMP3.getId3v1Tag();
	}
	
	private ID3v2 basculerID3v2(Mp3File fichierMP3) {
		if(fichierMP3.hasCustomTag())
			fichierMP3.removeCustomTag();
		if(fichierMP3.hasId3v1Tag())
			fichierMP3.removeId3v1Tag();
		if(!fichierMP3.hasId3v2Tag()) {
			ID3v24Tag id3Tag = new ID3v24Tag();
			fichierMP3.setId3v2Tag(id3Tag);
		}
		return fichierMP3.getId3v2Tag();
	}
	
	private void getAttributDepuisTag(Map<String, String> attributs, String attribut, String valeurAttribut) {
		if(attributs.get(attribut) != null
				&& !attributs.get(attribut).isEmpty())
			return;
		if(valeurAttribut != null && !valeurAttribut.isEmpty())
			attributs.put(attribut, valeurAttribut);
	}
	
	/**
	 * Renvoie le nom de la chanson en se basant sur le nom du fichier
	 * @param nomFichier
	 * @return
	 */
	private void getChansonDepuisNom(String nomFichier, Map<String, String> attributs) {
		if(nomFichier.contains(" - "))
			attributs.put(ATTRIBUT_CHANSON, 
					nomFichier
					.substring(3+nomFichier.indexOf(" - "))
					.replaceAll("[_]", " ")
					.replaceAll(".mp3", "")
					.trim()
					);
	}
	
	/**
	 * Renvoie le nom de l'artiste en se basant sur le nom du fichier
	 * @param nomFichier
	 * @return
	 */
	private void getArtisteDepuisNom(String nomFichier, Map<String, String> attributs) {
		if(nomFichier.contains(" - "))
			attributs.put(ATTRIBUT_ARTISTE, 
					nomFichier
					.substring(0, nomFichier.indexOf(" - "))
					.replaceAll("[_]", " ")
					.trim()
					);
	}
	
	/**
	 * Renvoie le nom du fichier, sans le chemin
	 * @param cheminEtNomFichier
	 * @return
	 */
	private String getNomFichier(String cheminEtNomFichier) {
		if(cheminEtNomFichier.contains("/"))
			return cheminEtNomFichier.substring(1+cheminEtNomFichier.lastIndexOf("/"));
		if(cheminEtNomFichier.contains("\\"))
			return cheminEtNomFichier.substring(1+cheminEtNomFichier.lastIndexOf("\\"));
		return cheminEtNomFichier;
	}
}
